import pygame.gfxdraw
import random


class Background(object):
    STAR_COUNT = 100
    STAR_COLOR = (255, 255, 255)


    def __init__(self, size):
        self._size = size
        self._star_positions = self._generate_stars()

    def draw(self, screen):
        for (star_x, star_y) in self._star_positions:
            pygame.gfxdraw.pixel(
                screen,
                star_x,
                star_y,
                Background.STAR_COLOR
            )

    def _generate_stars(self):
        star_positions = []
        for num in range(0, Background.STAR_COUNT):
            star_positions.append((
                random.randint(0, self._size[0]),
                random.randint(0, self._size[1])
            ))

        return star_positions
