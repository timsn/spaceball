import numpy


def multiply(x1, x2):
    return numpy.multiply(x1, x2)

def add(x1, x2):
    return numpy.add(x1, x2)

def subtract(x1, x2):
    return numpy.subtract(x1, x2)

def divide(x1, x2):
    return numpy.divide(x1, x2)

def magnitude(vector):
    return numpy.linalg.norm(vector)

def distance_vector(vector1, vector2):
    return subtract(vector1, vector2)

def normalize(distance_vector):
    vmag = magnitude(distance_vector)
    if vmag > 0:
        return (distance_vector[0]/vmag, distance_vector[1]/vmag)
    else:
        return (0.0, 0.0)

def unit_vector(pos1, pos2):
    dv = distance_vector(pos1, pos2)
    return normalize(dv)
