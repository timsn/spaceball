import collections

subscribers = collections.defaultdict(list)


def subscribe(event, subscriber):
    subscribers[event].append(subscriber)


def dispatch_event(event):
    for subscriber in subscribers[event.type]:
        subscriber.notify(event)
