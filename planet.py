import pygame
import os


class Planet(pygame.sprite.Sprite):
    COLOR = (234, 56, 12)
    TARGET_COLOR = (0, 0, 255)
    GRAVITY_FIELD = 150

    def __init__(self, position, gravity, radius, is_target, image_name):
        pygame.sprite.Sprite.__init__(self)
        self.position = position
        self.gravity = gravity
        self.radius = radius
        self.is_target = is_target

        self.image = pygame.image.load(os.path.join('assets/img', image_name)).convert()
        self.rect = self.image.get_rect()
        self.rect.move_ip(self.position)
        self.time_passed = 0

    def update(self, deltaTime):
        self.time_passed += deltaTime

    def draw(self, screen):
        screen.blit(self.image, self.rect)
        #pygame.draw.rect(screen, (255, 0, 0), self.rect, 1)
