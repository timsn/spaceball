import level
import physics
import background
import pygame
import finishscreen
import eventdispatcher
import sound
from shotcountdisplay import ShotCountDisplay
from planet import Planet
from ball import Ball
from pygame.locals import *


class World(object):

    def __init__(self, size):
        self.levelNumber = 0
        self.total_shot_count = 0
        self._background = background.Background(size)
        self.level = self._load_next_level()
        self._size = size
        self._finish_screen = None
        self.game_over = False
        self.quit = False
        eventdispatcher.subscribe(MOUSEBUTTONDOWN, self)

    def update(self, deltaTime):
        self.ball.update(deltaTime)
        for planet in self.planets:
            planet.update(deltaTime)
        self._collision_check()
        self._check_gravity()
        self._shot_count_display.update(deltaTime)

    def draw(self, screen):
        self._background.draw(screen)
        self.ball.draw(screen)
        for planet in self.planets:
            planet.draw(screen)
        if self._finish_screen:
            self._finish_screen.draw(screen)
        self._shot_count_display.draw(screen)

    def notify(self, event):
        if not self.game_over and self._finish_screen:
            self._finish_screen = None
            self.level = self._load_next_level()

    def _collision_check(self):
        self._world_boundary_check()
        self._planet_collision_check()

    def _planet_collision_check(self):
        crashgroup = pygame.sprite.spritecollide(
            self.ball,
            self.planets,
            False,
            pygame.sprite.collide_circle
        )
        if crashgroup and not self._finish_screen:
            for planet in crashgroup:
                if planet.is_target:
                    self._finish_screen = finishscreen.LevelFinishScreen(self.ball.shot_count)
                    self.ball.velocity = 0.0
                else:
                    self.ball.reset()
                    sound.play('fail1')

    def _check_gravity(self):
        for planet in self.planets:
            distance_vector = physics.distance_vector(
                planet.position,
                self.ball.position
            )
            magnitude = physics.magnitude(distance_vector)
            distance_threshold = (Planet.GRAVITY_FIELD + Ball.RADIUS + planet.radius)
            if magnitude < distance_threshold:
                self.ball.gravity_direction = physics.normalize(distance_vector)
                self.ball.gravity_pull = planet.gravity

    def _world_boundary_check(self):
        if (self.ball.position[0] < 0 or
                self.ball.position[0] > self._size[0] or
                self.ball.position[1] < 0 or
                self.ball.position[1] > self._size[1]):
            self.ball.reset()
            sound.play('fail2')

    def _load_next_level(self):
        if hasattr(self, 'level'):
            self.total_shot_count += self.ball.shot_count
        self.levelNumber += 1
        next_level = None
        try:
            next_level = level.Level(self.levelNumber)
            self.ball = next_level.ball
            self._shot_count_display = ShotCountDisplay(self.ball)
            self.planets = next_level.planets
        except level.NoMoreLevelsException as e:
            self.game_over = True
            self._finish_screen = finishscreen.GameFinishScreen(self.total_shot_count)

        return next_level
