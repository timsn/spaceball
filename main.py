import pygame
import eventdispatcher
import sound
from world import World
from pygame.locals import *

pygame.mixer.pre_init(44100, -16, 1, 512)
# Initialize Pygame
pygame.init()

frames_per_sec = 60.0

# Loop until the user clicks the close button.
running = True

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# Set the height and width of the screen
screen = pygame.display.set_mode([800, 600])

# Hide the mouse pointer
pygame.mouse.set_visible(False)

# Change window title
pygame.display.set_caption('Spaceball')

world = World(screen.get_size())

sound.load_sounds()


while running:
    # pygame.event.pump() # um get_pressed keys zu laden
    eventlist = pygame.event.get()
    for event in eventlist:
        eventdispatcher.dispatch_event(event)
        if (event.type == QUIT or
           (event.type == KEYDOWN and event.key == K_ESCAPE)):
            running = False

    #pygame.display.set_caption('Spaceball x:%d, y:%d' % (pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1]))

    deltaTime = clock.tick(frames_per_sec)

    world.update(deltaTime)

    screen.fill((0, 0, 0))

    world.draw(screen)

    # Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

pygame.quit()
