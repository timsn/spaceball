import os
import json
from ball import Ball
import planet


class Level(object):
    PATH = "assets/levels/"
    LEVELS_PREFIX = "level_"

    def __init__(self, number):
        filename = self._filename(number)
        self._load_from_file(filename)

    def _load_from_file(self, filename):
        try:
            level_data = json.load(open(filename))
            self._load_ball(level_data)
            self._load_planets(level_data)
        except IOError as e:
            raise NoMoreLevelsException("No more levels to load.")

    def _load_ball(self, level_data):
        ball_data = level_data.get('ball')
        self.ball = Ball((ball_data.get('x'), ball_data.get('y')))

    def _load_planets(self, level_data):
        planets_data = level_data.get('planets')
        self.planets = map(self._map_planets, planets_data)

    def _map_planets(self, planet_json):
        return planet.Planet(
            (planet_json.get('x'), planet_json.get('y')),
            planet_json.get('gravity'),
            planet_json.get('radius'),
            planet_json.get('is_target'),
            planet_json.get('image_name')
        )

    def _filename(self, number):
        return os.path.join(
            os.path.abspath('.'),
            Level.PATH,
            Level.LEVELS_PREFIX + str(number) + '.json'
        )

class NoMoreLevelsException(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)
