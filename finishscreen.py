import pygame
import physics
import sound

class FinishScreen(object):
    ALPHA_VALUE = 120
    TEXT_COLOR = (140, 180, 209)
    RESULT_TEXT_SIZE = 50
    SCORE_TEXT_SIZE = 30


    def title_text(self): pass
    def subtitle_text(self): pass

    def draw(self, screen):
        finish_screen = pygame.Surface(screen.get_size())
        finish_screen.fill((0, 0, 0))
        finish_screen.set_alpha(FinishScreen.ALPHA_VALUE)
        screen.blit(finish_screen, (0, 0))
        self._draw_title(screen)
        self._draw_subtitle(screen)

    def _draw_title(self, screen):
        fontname = pygame.font.get_default_font()
        text = self.title_text()
        font = pygame.font.SysFont(fontname, FinishScreen.RESULT_TEXT_SIZE)
        font_screen = font.render(text, False, FinishScreen.TEXT_COLOR)
        draw_position = physics.divide(screen.get_size(), 2)
        draw_position[0] -= font.size(text)[0]/2
        draw_position[1] -= font.size(text)[1]*2
        screen.blit(font_screen, draw_position)

    def _draw_subtitle(self, screen):
        fontname = pygame.font.get_default_font()
        text = self.subtitle_text()
        font = pygame.font.SysFont(fontname, FinishScreen.SCORE_TEXT_SIZE)
        font_screen = font.render(text, False, FinishScreen.TEXT_COLOR)
        draw_position = physics.divide(screen.get_size(), 2)
        draw_position[0] -= font.size(text)[0]/2
        draw_position[1] -= font.size(text)[1]/2
        screen.blit(font_screen, draw_position)


class LevelFinishScreen(FinishScreen):
    TITLE_TEXT = "Nice! You did it!"
    SUBTITLE_TEXT = "You took %d shot(s). Click to proceed."


    def __init__(self, shot_count):
        self._shot_count = shot_count
        sound.play('success')

    def title_text(self):
        return LevelFinishScreen.TITLE_TEXT

    def subtitle_text(self):
        return LevelFinishScreen.SUBTITLE_TEXT % self._shot_count


class GameFinishScreen(FinishScreen):
    TITLE_TEXT = "Congrats! You mastered all the levels!"
    SUBTITLE_TEXT = "Your total score (lower is better): %d"


    def __init__(self, total_shot_count):
        self._total_shot_count = total_shot_count
        sound.play('success')

    def title_text(self):
        return GameFinishScreen.TITLE_TEXT

    def subtitle_text(self):
        return GameFinishScreen.SUBTITLE_TEXT % self._total_shot_count

