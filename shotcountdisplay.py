import pygame


class ShotCountDisplay(object):
    TEXT_SIZE = 60
    TEXT_COLOR = (255, 255, 255)
    POSITION = (20, 20)

    def __init__(self, ball):
        self.ball = ball

    def update(self, deltaTime):
        self.shot_count = self.ball.shot_count

    def draw(self, screen):
        text = str(self.shot_count)
        fontname = pygame.font.get_default_font()
        font = pygame.font.SysFont(fontname, ShotCountDisplay.TEXT_SIZE)
        font_screen = font.render(text, False, ShotCountDisplay.TEXT_COLOR)
        screen.blit(font_screen, ShotCountDisplay.POSITION)
