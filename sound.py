import os
import pygame

sounds = {}


def load_sound(name):
    return pygame.mixer.Sound(os.path.join('assets/sound', name))


def load_sounds():
    sounds['success'] = load_sound('plop1.wav')
    sounds['fail1'] = load_sound('peng.wav')
    sounds['fail2'] = load_sound('knall.wav')
    sounds['release'] = load_sound('whoosh2.wav')


def play(name):
    if name in sounds:
        sounds[name].play()
    else:
        print("Error: sound not found")
