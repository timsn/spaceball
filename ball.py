import pygame
import eventdispatcher
import physics
import sound
import os
from pygame.locals import *


class Ball(object):
    COLOR = (255, 255, 255)
    RADIUS = 8
    ARROW_LENGTH = 30

    def __init__(self, position):
        self.origin = position
        self.shot_count = 0
        self.ready = False
        self.reset()
        eventdispatcher.subscribe(MOUSEBUTTONDOWN, self)
        self.rect = pygame.Rect(self.position[0], self.position[1], Ball.RADIUS * 2, Ball.RADIUS * 2)
        self.image = pygame.image.load(os.path.join('assets/img', 'ball.png')).convert()

    def notify(self, event):
        if event.type == MOUSEBUTTONDOWN and self.ready and self._is_at_starting_point():
            sound.play('release')
            self._shoot()

    def update(self, deltaTime):
        self.ready = True
        direction = physics.add(
            physics.multiply(self.direction, self.velocity),
            physics.multiply(self.gravity_direction, self.gravity_pull / 100)
        )
        normalized_direction = physics.normalize(direction)
        norm = physics.multiply(normalized_direction, self.velocity)
        newpos = physics.add(self.position, norm)
        self.direction = normalized_direction
        self.position = newpos
        self.rect = pygame.Rect(self.position[0] - Ball.RADIUS, self.position[1] - Ball.RADIUS, Ball.RADIUS * 2, Ball.RADIUS * 2)

    def reset(self):
        self.position = self.origin
        self.direction = (0.0, 0.0)
        self.gravity_direction = (0.0, 0.0)
        self.gravity_pull = 0.0
        self.velocity = 0.0

    def draw(self, screen):
        self._draw_arrow(screen)
        self._draw_ball(screen)
        #pygame.draw.rect(screen, (255, 0, 0), self.rect, 1)

    def _shoot(self):
        if self.velocity == 0:
            self.direction = physics.unit_vector(
                pygame.mouse.get_pos(),
                self.position
            )
            self.velocity = 4.0
            self.shot_count += 1

    def _draw_ball(self, screen):
        #pygame.draw.circle(
        #    screen,
        #    Ball.COLOR,
        #    self._integer_position(),
        #    Ball.RADIUS
        #)
        screen.blit(self.image, self.rect)

    def _integer_position(self):
        return (int(self.position[0]), int(self.position[1]))

    def _draw_arrow(self, screen):
        if self.velocity == 0 and self._is_at_starting_point():
            pygame.draw.line(
                screen,
                Ball.COLOR,
                self.position,
                self._get_line_end_position()
            )

    def _is_at_starting_point(self):
        return (self.position[0] == self.origin[0] and
                self.position[1] == self.origin[1])

    def _get_line_end_position(self):
        norm = physics.unit_vector(pygame.mouse.get_pos(), self.position)
        return physics.add(self.position, physics.multiply(norm, Ball.ARROW_LENGTH))
